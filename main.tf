provider "aws"{
    access_key = var.access_key
    secret_key = var.secret_key
    region = var.aws_region
}

#Create VPC
resource "aws_vpc" "quill_vpc" {
	cidr_block = var.cidr_block
	tags = {
    	Owner = "${var.owner}"
    	Name = "${var.owner}-vpc"
  	}
}

#Create subnet
resource "aws_subnet" "quill_subnet" {
	vpc_id            = aws_vpc.quill_vpc.id
	cidr_block        = "${var.sn_cidr_block}"
	availability_zone = "${var.aws_region}a"
	tags = {
    	Owner = "${var.owner}"
    	Name = "${var.owner}-subnet"
  	}
}

resource "aws_internet_gateway" "quill_gateway" {
	vpc_id = aws_vpc.quill_vpc.id
	tags = {
		Name = "${var.gateway_name}"
  	}
}

#Create security group with firewall rules
resource "aws_security_group" "quill_security_group" {
	name        = "${var.owner}-SG"
	description = "security group"
	vpc_id      = aws_vpc.quill_vpc.id
	ingress {
		from_port   = 8080
		to_port     = 8080
    	protocol    = "tcp"
    	cidr_blocks = ["0.0.0.0/0"]
	}
	ingress {
    	from_port   = 22
    	to_port     = 22
    	protocol    = "tcp"
    	cidr_blocks = ["0.0.0.0/0"]
	}
	# outbound from server
  	egress {
    	from_port   = 0
    	to_port     = 65535
    	protocol    = "tcp"
    	cidr_blocks = ["0.0.0.0/0"]
	}
}

#Create Network interface
resource "aws_network_interface" "quill_network" {
	subnet_id   = aws_subnet.quill_subnet.id
	private_ips = ["172.16.10.100"]
	security_groups = ["${aws_security_group.quill_security_group.id}"]
}

#Create EC2 Instance
resource "aws_instance" "quill_EC2" {
	ami				= "${var.instance_ami}"
	key_name		= "${var.key_name}"
	instance_type 	= "${var.instance_type}"
	availability_zone = "${var.aws_region}a"
	network_interface {
		network_interface_id = aws_network_interface.quill_network.id
    	device_index         = 0
	}
	tags = {
    	"Owner" = var.owner
    	"Name"  = "${var.owner}-ec2"
  	}
}

# Create a new load balancer
resource "aws_elb" "quill_ELB" {
	name	= "${var.elb_name}"
	subnets 	= ["${aws_subnet.quill_subnet.id}"]
	# access_logs {
	# 	bucket        = "foo"
    # 	bucket_prefix = "bar"
    # 	interval      = 60
	# }
  	listener {
    	instance_port     = 8000
    	instance_protocol = "http"
    	lb_port           = 80
    	lb_protocol       = "http"
	}
  	# listener {
    # 	instance_port      = 8000
    # 	instance_protocol  = "http"
    # 	lb_port            = 443
    # 	lb_protocol        = "https"
    # 	ssl_certificate_id = "arn:aws:iam::123456789012:server-certificate/certName"
  	# }
  	health_check {
    	healthy_threshold   = 2
    	unhealthy_threshold = 2
    	timeout             = 3
    	target              = "${var.elb_healthcheck}"
    	interval            = 30
  	}
  	instances                   = [aws_instance.quill_EC2.id]
  	cross_zone_load_balancing   = true
  	idle_timeout                = 400
  	connection_draining         = true
  	connection_draining_timeout = 400
}

#Create Target Group
resource "aws_lb_target_group" "quill_TG" {
	name     = "${var.owner}-tg"
	port     = 80
	protocol = "HTTP"
	vpc_id   = aws_vpc.quill_vpc.id
}

// Attach the target groups to the instance(s)
resource "aws_lb_target_group_attachment" "quill_tgr_attachment" {
	target_group_arn = aws_lb_target_group.quill_TG.arn
	target_id        = aws_instance.quill_EC2.id
	port             = 80
}

resource "aws_iam_role" "codedeploy" {
	name = "example-codedeploy"

	assume_role_policy = jsonencode({
  		"Version": "2012-10-17",
  		"Statement": [
    	{
      		"Sid": "",
      		"Effect": "Allow",
		    "Principal": {
        		"Service": "codedeploy.amazonaws.com"
      		},
      	"Action": "sts:AssumeRole"
    	}]
	})
}

resource "aws_iam_role_policy_attachment" "AWSCodeDeployRole" {
	policy_arn = "arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole"
	role       = aws_iam_role.codedeploy.name
}

// Create Application
resource "aws_codedeploy_app" "quill_app" {
	compute_platform = "Server"
	name             = "${var.app_name}"
}

//Create Deployment Group
resource "aws_codedeploy_deployment_group" "Quill_deploymentgroup" {
	app_name               = "${var.app_name}"
	deployment_group_name  = "${var.deployment_group_name}"
	service_role_arn       = "${aws_iam_role.codedeploy.arn}"
	deployment_config_name = "CodeDeployDefault.AllAtOnce"

	ec2_tag_set {
    	ec2_tag_filter {
      		key   = "Name"
      		type  = "KEY_AND_VALUE"
      		value = "${var.owner}-ec2"
    	}
  	}

  	# # You can configure the Load Balancer to use in a deployment.
  	# load_balancer_info {
    # 	# Information about two target groups and how traffic routes during an Amazon ECS deployment.
    # 	# An optional test traffic route can be specified.
    # 	# https://docs.aws.amazon.com/codedeploy/latest/APIReference/API_TargetGroupPairInfo.html
    # 	target_group_pair_info {
    # 	  	# The path used by a load balancer to route production traffic when an Amazon ECS deployment is complete.
    #   		prod_traffic_route {
    #     	listener_arns = ["${var.lb_listener_arns}"]
    #   		}

    #   		# One pair of target groups. One is associated with the original task set.
    #   		# The second target is associated with the task set that serves traffic after the deployment completes.
    #   		target_group {
    #     		name = "${var.blue_lb_target_group_name}"
    #   		}

    #   		target_group {
    #     		name = "${var.green_lb_target_group_name}"
    #   		}

    #   		# An optional path used by a load balancer to route test traffic after an Amazon ECS deployment.
    #   		# Validation can happen while test traffic is served during a deployment.
    #   		test_traffic_route {
    #     		listener_arns = ["${var.test_traffic_route_listener_arns}"]
    #   		}
    # 	}
  	# }
}	