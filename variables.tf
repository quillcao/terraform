variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "us-east-2"
}

variable "access_key"{
    default = "AKIAS6CS6FETWAB4H6HO"
}

variable "secret_key"{
    default = "9hCzfugFcj/PZ2t8h09LtmwAVVPcz/+1wHiNTF8b"
}

variable "owner" {
    description = "Configuration owner"
    type        = string
    default = "quill"
}

variable "cidr_block"{
    default = "172.16.0.0/16"
}

variable "sn_cidr_block"{
    default = "172.16.10.0/24"
}

variable "aws_region_az" {
  description = "AWS region availability zone"
  type        = string
  default     = "a"
}

variable "key_name" {
  description = " SSH keys to connect to ec2 instance"
  default     =  "heric"
}

variable "instance_type" {
  description = "instance type for ec2"
  default     =  "t2.micro"
}
variable "instance_ami" {
  description = "ID of the AMI used"
  type        = string
  default     = "ami-0443305dabd4be2bc"
}



variable "app_name" {
    default     =  "quill_app"
}

variable "deployment_group_name" {
    default     =  "quill_deploymentgroup"
}

variable "elb_name" {
    default     =  "quill-elb"
}
variable "elb_healthcheck"{
     default = "HTTP:8080/"
}

variable "gateway_name" {
    default     =  "quill-gw"
}